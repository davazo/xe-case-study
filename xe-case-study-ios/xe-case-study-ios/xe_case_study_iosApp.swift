//
//  xe_case_study_iosApp.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 3/7/21.
//

import SwiftUI

@main
struct xe_case_study_iosApp: App {
    var body: some Scene {
        WindowGroup {
            NewPropertyView()
        }
    }
}
