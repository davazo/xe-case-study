//
//  ContentView.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 3/7/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
