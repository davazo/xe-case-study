//
//  NewPropertyUC.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 5/7/21.
//
import Combine
import Foundation

protocol INewPropertyUC {
    func fetchLocations(searchQuery: String) -> AnyPublisher<[Location], ApiError>
}

class NewPropertyUC: INewPropertyUC {
    private var repo: INewPropertyRepo
    init() {
        repo = NewPropertyRepo()
    }

    func fetchLocations(searchQuery: String) -> AnyPublisher<[Location], ApiError> {
        repo.fetchLocations(searchQuery: searchQuery)
    }
}
