//
//  NewPropertyVM.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 3/7/21.
//

import Combine
import Foundation

class NewPropertyVM: ObservableObject {
    func validationForm(property: Property) -> Bool {
        if !property.title.isEmpty, !property.location.isEmpty {
            return false
        } else {
            return true
        }
    }

    func submitNewProperty(propertyInfo: Property) -> String {
        do {
            let jsonData = try JSONEncoder().encode(propertyInfo)
            let jsonString = String(data: jsonData, encoding: .utf8)!
            return jsonString
        } catch {
            print(error)
            return "error encode property info"
        }
    }
}
