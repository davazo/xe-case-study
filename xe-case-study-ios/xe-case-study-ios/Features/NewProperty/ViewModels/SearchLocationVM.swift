//
//  SearchLocationVM.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 3/7/21.
//

import Combine
import Foundation

class SearchLocationVM: ObservableObject {
    @Published var searchText = ""
    @Published var searching = false
    @Published var locations: [Location] = []

    private var newPropertyUC: INewPropertyUC
    private var searchTextPublisher: AnyCancellable?
    private var cancellable: AnyCancellable?

    init() {
        newPropertyUC = NewPropertyUC()
        searchTextPublisher = $searchText
            .debounce(for: 0.3, scheduler: DispatchQueue.main)
            .sink(receiveValue: { _ in
                print("Search Text Combine")
                if self.searchText.count > 2 {
                    self.fetchLocations(searchQuery: self.searchText)
                } else {
                    self.searching = false
                    self.locations = []
                }
            })
    }

    func fetchLocations(searchQuery: String) {
        searching = true
        cancellable = newPropertyUC.fetchLocations(searchQuery: searchQuery)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { error in
                print("\(String(describing: error))")
            }, receiveValue: { value in
                self.locations = value
                self.searching = false
            })
    }
}
