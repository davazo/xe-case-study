//
//  Property.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 3/7/21.
//

import Foundation

struct Property: Encodable {
    var title: String
    var location: String
    var price: String
    var description: String
}
