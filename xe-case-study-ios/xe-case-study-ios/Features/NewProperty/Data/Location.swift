//
//  Location.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 5/7/21.
//

import Foundation

class Locations {
    let locations: [Location]

    init(locations: [Location]) {
        self.locations = locations
    }
}

struct Location: Codable {
    var placeId: String
    var mainText: String
    var secondaryText: String
}
