//
//  NewPropertyRepo.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 5/7/21.
//

import Combine
import Foundation

protocol INewPropertyRepo {
    func fetchLocations(searchQuery: String) -> AnyPublisher<[Location], ApiError>
}

class NewPropertyRepo: INewPropertyRepo {
    private var remote: INewPropertyRemote
    private var newProperyCache: INewPropertyCache
    init() {
        remote = NewPropertyRemote()
        newProperyCache = NewPropertyCache()
    }

    func fetchLocations(searchQuery: String) -> AnyPublisher<[Location], ApiError> {
        let cachedLocations = newProperyCache.retrieve(searchQuery: searchQuery)
        if !cachedLocations.isEmpty {
            return Just(cachedLocations).setFailureType(to: ApiError.self).eraseToAnyPublisher()
        } else {
            return remote.get(searchQuery: searchQuery)
                .flatMap { [weak self] locations -> AnyPublisher<[Location], ApiError> in
                    self?.newProperyCache.store(searchQuery: searchQuery, locations: locations)
                    return Just(locations).setFailureType(to: ApiError.self).eraseToAnyPublisher()
                }.eraseToAnyPublisher()
        }
    }
}
