//
//  MainPageView.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 3/7/21.
//

import Combine
import SwiftUI

struct NewPropertyView: View {
    init() {
        UINavigationBar.appearance().largeTitleTextAttributes = [.font: UIFont(name: "GeezaPro-Bold", size: 20)!]
    }

    @ObservedObject var vm = NewPropertyVM()
    @State var propertyInfo = Property(title: "", location: "", price: "", description: "")

    @State private var showingSearchLocation = false
    @State var showingAlert = false
    var body: some View {
        NavigationView {
            ZStack {
                Color.caseStudy.background.edgesIgnoringSafeArea(.all)
                ScrollView(showsIndicators: false) {
                    TextInput(title: "Title", placeholder: "Type something", text: $propertyInfo.title)
                        .padding()

                    Button(action: {
                        showingSearchLocation.toggle()
                    }) {
                        TextInput(title: "Location", placeholder: "Select Location", text: $propertyInfo.location).disabled(true)
                            .foregroundColor(Color.caseStudy.black)
                    }
                    .padding()
                    .sheet(isPresented: $showingSearchLocation) {
                        SearchLocationView(selectedLocation: $propertyInfo.location)
                    }

                    TextInput(title: "Price", placeholder: "Type something", text: $propertyInfo.price)
                        .padding()

                    TextEditorInput(title: "Description", text: $propertyInfo.description).padding()
                        .frame(height: 200)

                    HStack(spacing: 20) {
                        SumbitButton()
                        ClearTextfieldButton()
                    }.padding()
                }
            }
            .navigationTitle("New Property Classified")
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

struct MainPageView_Previews: PreviewProvider {
    static var previews: some View {
        NewPropertyView()
    }
}
