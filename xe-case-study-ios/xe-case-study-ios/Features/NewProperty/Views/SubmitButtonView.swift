//
//  SubmitButtonView.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 6/7/21.
//

import SwiftUI

extension NewPropertyView {
    @ViewBuilder func SumbitButton() -> some View {
        Button(action: {
            showingAlert = true
        }) {
            ButtonLabel(title: "Submit", color: Color.caseStudy.green)
                .opacity(vm.validationForm(property: propertyInfo) ? 0.3 : 1)
        }
        .disabled(vm.validationForm(property: propertyInfo))
        .alert(isPresented: $showingAlert) {
            Alert(title: Text(vm.submitNewProperty(propertyInfo: propertyInfo)), message: Text("New property"), dismissButton: .default(Text("Close!")))
        }
    }

    @ViewBuilder func ClearTextfieldButton() -> some View {
        Button(action: {
            self.propertyInfo.title = ""
            self.propertyInfo.location = ""
            self.propertyInfo.price = ""
            self.propertyInfo.description = ""
        }) {
            ButtonLabel(title: "clear", color: Color.caseStudy.red)
        }
    }
}
