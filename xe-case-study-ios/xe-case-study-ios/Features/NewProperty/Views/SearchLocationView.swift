//
//  SearchLocationView.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 3/7/21.
//

import SwiftUI

struct SearchLocationView: View {
    @Environment(\.presentationMode) var presentationMode

    @ObservedObject var vm = SearchLocationVM()
    @State private var searchQuery: String = ""
    @Binding var selectedLocation: String

    let layout = [
        GridItem(.flexible())
    ]

    var body: some View {
        ZStack {
            Color.caseStudy.background
            VStack {
                HStack {
                    Button(
                        action: {
                            presentationMode.wrappedValue.dismiss()
                        },
                        label: {
                            Image(systemName: "arrowshape.turn.up.left.circle.fill")
                                .imageScale(.large)
                                .padding(4)
                        }
                    )

                    CustomTextField(text: $vm.searchText, isFirstResponder: true)
                        .frame(width: 220, height: 30)
                        .padding(7)
                        .padding(.horizontal, 10)
                        .background(Color.caseStudy.background)

                        .cornerRadius(8)

                    if vm.searchText != "" {
                        Button(
                            action: {
                                self.vm.searchText = ""

                            },
                            label: {
                                Image(systemName: "xmark.circle.fill").imageScale(.large)
                                    .padding(6)
                            }
                        )
                    }
                    if vm.searching {
                        ProgressView()
                    }
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding()
                .background(Color.caseStudy.white)

                if vm.locations.count > 0 {
                    ScrollView(showsIndicators: false) {
                        LazyVGrid(columns: layout, alignment: .leading, spacing: 12) {
                            ForEach(vm.locations, id: \.placeId) { location in
                                Button(
                                    action: {
                                        self.selectedLocation = location.mainText
                                        presentationMode.wrappedValue.dismiss()
                                    },
                                    label: {
                                        Text(location.mainText)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .foregroundColor(Color.caseStudy.black)
                                    }
                                )
                            }
                            .padding(.top)
                            .padding(.horizontal)
                        }
                    }
                    .background(Color.caseStudy.white)
                    .cornerRadius(8.0, antialiased: true)
                    .padding(.horizontal, 10)
                    .animation(.default)

                } else {
                    Spacer()
                    if vm.searchText == "" {
                        Text("Search area")
                    } else {
                        Text("No results")
                    }
                    Spacer()
                }
            }
        }
    }

    struct SearchLocationView_Previews: PreviewProvider {
        static var previews: some View {
            SearchLocationView(selectedLocation: .constant(""))
        }
    }
}

struct CustomTextField: UIViewRepresentable {
    class Coordinator: NSObject, UITextFieldDelegate {
        @Binding var text: String
        var didBecomeFirstResponder = false

        init(text: Binding<String>) {
            _text = text
        }

        func textFieldDidChangeSelection(_ textField: UITextField) {
            text = textField.text ?? ""
        }
    }

    @Binding var text: String
    var isFirstResponder: Bool = false

    func makeUIView(context: UIViewRepresentableContext<CustomTextField>) -> UITextField {
        let textField = UITextField(frame: .zero)
        textField.delegate = context.coordinator
        return textField
    }

    func makeCoordinator() -> CustomTextField.Coordinator {
        return Coordinator(text: $text)
    }

    func updateUIView(_ uiView: UITextField, context: UIViewRepresentableContext<CustomTextField>) {
        uiView.text = text
        if isFirstResponder, !context.coordinator.didBecomeFirstResponder {
            uiView.becomeFirstResponder()
            context.coordinator.didBecomeFirstResponder = true
        }
    }
}
