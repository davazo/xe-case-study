//
//  NewPropertyCache.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 6/7/21.
//

import Combine
import Foundation

protocol INewPropertyCache {
    func retrieve(searchQuery: String) -> [Location]
    func store(searchQuery: String, locations: [Location])
}

let cache = NSCache<NSString, Locations>()

class NewPropertyCache: INewPropertyCache {
    func retrieve(searchQuery: String) -> [Location] {
        if let cachedLocations = cache.object(forKey: searchQuery as NSString) {
            return cachedLocations.locations
        } else {
            return []
        }
    }

    func store(searchQuery: String, locations: [Location]) {
        let objectLocations = Locations(locations: locations)
        cache.setObject(objectLocations, forKey: searchQuery as NSString)
    }
}
