//
//  NewPropertyRemote.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 5/7/21.
//
import Combine
import Foundation

protocol INewPropertyRemote {
    func get(searchQuery: String) -> AnyPublisher<[Location], ApiError>
}

class NewPropertyRemote: INewPropertyRemote {
    private var restClient: RestClient

    init() {
        restClient = RestClient()
    }

    func get(searchQuery: String) -> AnyPublisher<[Location], ApiError> {
        return restClient.call(Endpoint.locations(searchQuery: searchQuery), method: .GET)
    }
}
