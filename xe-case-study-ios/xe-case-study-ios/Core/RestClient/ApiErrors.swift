//
//  Errors.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 5/7/21.
//

import Foundation

enum ApiError: Error {
    case internalError
    case serverError
    case parsingError
}
