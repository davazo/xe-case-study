//
//  RestClient.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 5/7/21.
//

import Combine
import Foundation

enum Method: String {
    case GET
}

class RestClient {
    private func request(for path: Endpoint, method: Method) -> URLRequest {
        var request = URLRequest(url: path.url)
        request.httpMethod = "\(method)"
        request.allHTTPHeaderFields = ["Content-Type": "application/json"]
        return request
    }

    private func dataTask(urlRequest: URLRequest) -> AnyPublisher<Data, ApiError> {
        return URLSession.shared.dataTaskPublisher(for: urlRequest)
            .mapError { _ in ApiError.serverError }
            .map { $0.data }
            .eraseToAnyPublisher()
    }

    func call<T: Codable>(_ path: Endpoint, method: Method) -> AnyPublisher<[T], ApiError> {
        let urlRequest = request(for: path, method: method)

        return dataTask(urlRequest: urlRequest)
            .flatMap { [weak self] data -> AnyPublisher<[T], ApiError> in
                do {
                    let locations = try JSONDecoder().decode([T].self, from: data)
                    return Just(locations).setFailureType(to: ApiError.self).eraseToAnyPublisher()
                } catch {
                    return Just([]).setFailureType(to: ApiError.self).eraseToAnyPublisher()
                }
            }
            .eraseToAnyPublisher()
    }
}
