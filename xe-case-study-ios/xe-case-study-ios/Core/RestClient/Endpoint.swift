//
//  Endpoint.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 5/7/21.
//

import Foundation

enum Endpoint {
    private var baseUrl: URL {
        return URL(string: "https://xegr-geography.herokuapp.com/places/autocomplete")!
    }

    var url: URL {
        if let encodePath = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            return URL(string: encodePath, relativeTo: baseUrl)!
        } else {
            return URL(string: "")!
        }
    }

    case locations(searchQuery: String)
    var path: String {
        switch self {
        case let .locations(searchQuery): return "?input=\(searchQuery)"
        }
    }
}
