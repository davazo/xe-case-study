//
//  TextEditorInput.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 4/7/21.
//

import SwiftUI

struct TextEditorInput: View {
    var title: String
    @Binding var text: String
    var body: some View {
        VStack(alignment: .leading) {
            Text(title)
            TextEditor(text: $text)
                .padding(10)
                .padding(.horizontal, 4)
                .background(Color.caseStudy.white)
                .cornerRadius(8)
                .lineSpacing(5)
        }
    }
}

struct TextEditorInput_Previews: PreviewProvider {
    static var previews: some View {
        TextEditorInput(title: "Description", text: .constant(""))
    }
}
