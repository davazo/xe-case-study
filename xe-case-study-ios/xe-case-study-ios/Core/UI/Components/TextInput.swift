//
//  TextInput.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 4/7/21.
//

import SwiftUI

struct TextInput: View {
    var title: String
    var placeholder: String
    @Binding var text: String

    var body: some View {
        VStack(alignment: .leading) {
            Text(title).foregroundColor(Color.caseStudy.black)
            TextField(placeholder, text: $text)
                .padding(10)
                .padding(.horizontal, 4)
                .background(Color.caseStudy.white)
                .cornerRadius(8)
        }
    }
}

struct TextInput_Previews: PreviewProvider {
    static var previews: some View {
        TextInput(title: "Title", placeholder: "Type something", text: .constant(""))
    }
}
