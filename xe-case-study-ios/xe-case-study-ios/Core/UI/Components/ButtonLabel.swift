//
//  ButtonLabel.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 4/7/21.
//

import SwiftUI

struct ButtonLabel: View {
    var title: String
    var color: Color
    var body: some View {
        Text(title)
            .padding(.horizontal, 15)
            .padding(.vertical, 8)
            .background(color)
            .foregroundColor(Color.caseStudy.black)
            .cornerRadius(4)
    }
}

struct ButtonLabel_Previews: PreviewProvider {
    static var previews: some View {
        ButtonLabel(title: "Button", color: Color.caseStudy.green)
    }
}
