//
//  Colors.swift
//  xe-case-study-ios
//
//  Created by Anestis Konstantinidis on 3/7/21.
//

import SwiftUI

protocol ICaseStudyColors {
    static var background: Color { get }
    static var black: Color { get }
    static var blue: Color { get }
    static var red: Color { get }
    static var white: Color { get }
    static var green: Color { get }
}

private struct CaseStudyCollors: ICaseStudyColors {
    static var background = Color("background")
    static var black = Color("black")
    static var blue = Color("blue")
    static var red = Color("red")
    static var white = Color("white")
    static var green = Color("green")
}

extension Color {
    static var caseStudy: ICaseStudyColors.Type {
        return CaseStudyCollors.self
    }
}
